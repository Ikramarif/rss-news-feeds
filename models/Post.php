<?php 
  class Post 
  {
    private $conn;
    private $table = 'feeds';
    public  $updatestatus = "";
    public  $title;
    public  $date;
    public  $link;
    public  $description;
    public  $pname;
    public  $providername;
    public  $provider_link;
    public  $provider_lastupdated;
    public  $fstatus;
    public  $fname;
    
    public function __construct($db) 
    {
      $this->conn = $db;
    }

    public function read() 
    {
      $result  = '';
      $host    = 'localhost';
      $db      = 'databaseproject';
      $user    = 'root';
      $pass    = '';
      $charset = 'utf8mb4';
      $options = [
          \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
          \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
          \PDO::ATTR_EMULATE_PREPARES   => false,
      ];
      $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
      try 
      {
        $pdo = new \PDO($dsn, $user, $pass, $options);
        $result = $pdo->query("SELECT title, descr, link, date, provider_name, date_created FROM feeds WHERE active=1")->fetchAll();
      } 
      catch (\PDOException $e) 
      {
          throw new \PDOException($e->getMessage(), (int)$e->getCode());
      }
      return $result;
    }
     
    public function readProviders() 
    {
      $query = 'SELECT `provider_name`, `provider_link`, `provider_lastupdated`, `provider_lastattempt`, `size`, `feed_format`, `error`, `active` FROM `feeds_providers`';
      $stmt  = $this->conn->prepare($query);
      $stmt->execute();
      return $stmt;
    }

    public function readenabledProviders() 
    {
      $query = 'SELECT `provider_name`, `provider_link`, `provider_lastupdated`, `provider_lastattempt`, `size`, `feed_format`, `error`, `active` FROM `feeds_providers` WHERE `active`=0';
      $stmt  = $this->conn->prepare($query);
      $stmt->execute();
      return $stmt;
    }

    public function updatefeeds() 
    {
      global $updatestatus;
      $servername = "localhost";
      $username   = "root";
      $password   = "";
      $db   = "databaseproject";
      $conn = new mysqli($servername, $username, $password, $db);	
      if ($conn->connect_error) 
      {
        die("Connection failed: " . $conn->connect_error);
      }
      $currentdatetemp = date('Y-m-d H:i:s');
      $query2   = "UPDATE `feeds_providers` SET `provider_lastattempt` = NOW() ";
      $result2  = $conn->query($query2);
      $query    = 'SELECT * FROM `feeds_providers`';
      $result   = mysqli_query($conn, $query);
      if (mysqli_num_rows($result) > 0) 
      {
        while($row = mysqli_fetch_assoc($result)) 
        {
          $currenttime = new DateTime(date('Y-m-d H:i:s'));
          $latupdated  = new DateTime($row["provider_lastupdated"]);
          $interval    = $currenttime->diff($latupdated);
          $minutes     = intval($interval->format('%i'));
          $seconds     = intval($interval->format('%s'));
          if($minutes>10)
          {
            if($row["feed_format"]=="RSS")
            {
              $feedname = $row["provider_name"];
              $feedurl  = $row["provider_link"];
              $rss      = new DOMDocument();
              $rss->load($feedurl);
              $feed     = array();
              foreach ($rss->getElementsByTagName('item') as $node) 
              {
                  $item = array ( 
                      'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
                      'descr' => $node->getElementsByTagName('description')->item(0)->nodeValue,
                      'link'  => $node->getElementsByTagName('link')->item(0)->nodeValue,
                      'date'  => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
                      );
                  array_push($feed, $item);
              }
              $limit = sizeof($feed);
              echo $limit;
              for($x=0;$x<$limit;$x++) 
              {
                $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
                $link = $feed[$x]['link'];
                $descr = $feed[$x]['descr'];
                $tdate = date('Y-m-d', strtotime($feed[$x]['date']));
                $currentdate = date('Y-m-d H:i:s');
    
                if (empty($descr)||empty($tdate)||empty($title)||empty($link)) 
                {
                  continue;
                }
                $host    = 'localhost';
                $db      = 'databaseproject';
                $user    = 'root';
                $pass    = '';
                $charset = 'utf8mb4';
                $options = [
                    \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                    \PDO::ATTR_EMULATE_PREPARES   => false,
                ];
                $dsn = "mysql:host=$host;dbname=$db;charset=$charset";                
                $pdo = new \PDO($dsn, $user, $pass, $options);
                $data = [
                    'title' => $title,
                    'descr' => $descr,
                    'link'  => $link,
                    'date'  => $tdate,
                    'provider_name' => $feedname,
                    'date_created'  => $currentdate,
                    'date_updated'  => $currentdate,
                ];
                try
                {
                  $sql = "UPDATE feeds_providers SET provider_lastattempt=? WHERE provider_name=?";
                  $pdo->prepare($sql)->execute([$currentdate,$feedname]);
  
                  $sql = "UPDATE feeds_providers SET size=? WHERE provider_name=?";
                  $pdo->prepare($sql)->execute([$limit,$feedname]);
  
                  $sql = "INSERT INTO feeds (title, descr, link, date, provider_name, date_created, date_updated) VALUES 
                  (:title, :descr, :link, :date, :provider_name, :date_created, :date_updated)";
                  $stmt= $pdo->prepare($sql);
                  $stmt->execute($data);
                }
                catch(PDOexception $ex)
                {
                  if ($ex->getCode() != 23000)
                  {
                    $sql = "UPDATE feeds_providers SET error=? WHERE provider_name=?";
                    $pdo->prepare($sql)->execute([$ex,$feedname]);
                    throw $ex;
                  }
                  else
                  {
                    //echo "Already Exist";
                  }
                }
                $sql = "UPDATE feeds_providers SET provider_lastupdated=? WHERE provider_name=?";
                $pdo->prepare($sql)->execute([$currentdate,$feedname]);
              }          
            }
            else if($row["feed_format"]=="ATOM")
            {
              $feedname = $row["provider_name"];
              $feedurl = $row["provider_link"];
              $url = ''.$feedurl;
              $feed = simplexml_load_file($url);
              $limit = sizeof($feed);
              $flag = 1;
        
              foreach($feed->entry as $item)
              {
                if($flag <= $limit)
                {
                  $qtitle        = $item->title;
                  $qlink         = $item->id;
                  $qdate         = date('Y-m-d', strtotime($item->updated));
                  $qdescr        = $item->content;
                  $currentdate   = date('Y-m-d H:i:s');
    
                  if (empty($qdescr)||empty($qdate)||empty($qtitle)||empty($qlink)) 
                  {
                    continue;
                  }
                  $host = 'localhost';
                  $db   = 'databaseproject';
                  $user = 'root';
                  $pass = '';
                  $charset = 'utf8mb4';
                  $options = [
                      \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                      \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                      \PDO::ATTR_EMULATE_PREPARES   => false,
                  ];
                  $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
                  
                  $pdo = new \PDO($dsn, $user, $pass, $options);
                  $data = [
                      'title' => $qtitle,
                      'descr' => $qdescr,
                      'link'  => $qlink,
                      'date'  => $qdate,
                      'provider_name' => $feedname,
                      'date_created'  => $currentdate,
                      'date_updated'  => $currentdate,
                  ];
                  try
                  {        
                    $sql = "UPDATE feeds_providers SET provider_lastattempt=? WHERE provider_name=?";
                    $pdo->prepare($sql)->execute([$currentdate,$feedname]);
  
                    $sql = "UPDATE feeds_providers SET size=? WHERE provider_name=?";
                    $pdo->prepare($sql)->execute([$limit,$feedname]);
  
                    $sql = "INSERT INTO feeds (title, descr, link, date, provider_name, date_created, date_updated) VALUES 
                    (:title, :descr, :link, :date, :provider_name, :date_created, :date_updated)";
                    $stmt= $pdo->prepare($sql);
                    $stmt->execute($data);
                  }
                  catch(PDOexception $ex)
                  {
                    if ($ex->getCode() != 23000)
                    {
                        $sql = "UPDATE feeds_providers SET error=? WHERE provider_name=?";
                        $pdo->prepare($sql)->execute([$ex,$feedname]);
                        throw $ex;
                    }
                    else{
                        //echo "Already Exist";
                    }
                  }
                  $sql = "UPDATE feeds_providers SET provider_lastupdated=? WHERE provider_name=?";
                  $pdo->prepare($sql)->execute([$currentdate,$feedname]);
                }
                $flag++;
              }
            }
            $updatestatus .= "" .$row["provider_name"]. " was updated Successfully. " ;
          }
          else
          {
            $diff = 11-$minutes;
            $updatestatus .= "" .$row["provider_name"]. " : Try again in approximately " .$diff. " minutes.";
          }
        }
        return $updatestatus;
      } 
      else 
      {
        echo "No Providers Found";
      }
    }

    public function updateprovidersstatus()
    {
      $host = 'localhost';
      $db_name = 'databaseproject';
      $username = 'root';
      $password = '';
      $conn;
      try 
      { 
        $this->conn = new PDO('mysql:host=' . $host . ';dbname=' . $db_name, $username, $password);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } 
      catch(PDOException $e) {
        echo 'Connection Error: ' . $e->getMessage();
      }
      $tablename = "feeds_providers";
      $taablename = "feeds";
      try{
        $query = 'UPDATE ' . $tablename . '
                  SET active = :active
                  WHERE provider_name = :provider_name';
        
        $query2 = 'UPDATE ' . $taablename . '
        SET active = :active
        WHERE provider_name = :provider_name';
                  
        $stmt = $this->conn->prepare($query);
        $stmt2 = $this->conn->prepare($query2);
        $stmt->bindParam(':active', $this->fstatus);
        $stmt->bindParam(':provider_name', $this->fname);
        $stmt2->bindParam(':active', $this->fstatus);
        $stmt2->bindParam(':provider_name', $this->fname);
        if($stmt->execute() && $stmt2->execute()) {
          return true;
        }
      }catch (PDOexception $ex)
      {
        return $ex;
      }
    }
  }
?>