<?php 
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  include_once '../../config/Database.php';
  include_once '../../models/Post.php';

  $database = new Database();
  $db = $database->connect();
  $post = new Post($db);
  $result = $post->read();
  $num = 1;
  
  if($num > 0) 
  {
    $posts_arr = array();
    foreach ($result as $row)
    {
      $post_item = array(
        'title' => $row['title'],
        'descr' => $row['descr'],        
        'link' => $row['link'],
        'date' => $row['date'],
        'provider_name' => $row['provider_name'],
        'date_created' => $row['date_created']
      );
      array_push($posts_arr, $post_item);
    }
    
    echo json_encode($posts_arr);

  } else {
    echo json_encode(
      array('message' => 'No Posts Found')
    );
  }
