<?php 
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  include_once '../../config/Database.php';
  include_once '../../models/Post.php';

  $database = new Database();
  $db = $database->connect();
  $post = new Post($db);
  $result = $post->readProviders();
  $num = $result->rowCount();

  if($num > 0) {
    $posts_arr = array();

    while($row = $result->fetch(PDO::FETCH_ASSOC)) 
    {
      extract($row);
      $post_item = array(
        'provider_name' => $provider_name,
        'provider_link' => $provider_link,        
        'provider_lastupdated' => $provider_lastupdated,
        'feed_format'=> $feed_format,
        'provider_lastattempt' => $provider_lastattempt,
        'error' => $error,
        'size' => $size,
        'active' => $active
      );

      array_push($posts_arr, $post_item);
    }
    echo json_encode($posts_arr);
  } 
  else {
    echo json_encode(
      array('message' => 'No Posts Found')
    );
  }
