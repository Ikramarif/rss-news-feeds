var json;
var jsonproviders;
var jsonupdate;
document.addEventListener('DOMContentLoaded', function () 
{
    var req;
    req = new XMLHttpRequest();
    req.open("GET", 'http://localhost:8080/DatabaseAndWebTechnique/api/post/read.php', true);
    req.send();

    var request;
    request = new XMLHttpRequest();
    request.open("GET", 'http://localhost:8080/DatabaseAndWebTechnique/api/post/readProviders.php', true);
    request.send();

    req.onload = function () {
        json = JSON.parse(req.responseText);
        printData(json,"all");
    };

    request.onload = function () {
        jsonproviders = JSON.parse(request.responseText);
        printproviders(jsonproviders);  
    };
        
});
function feedsclick(){
    printData(json,"all");
}
function thelocalclick(){
    printData(json,"The Local");
}
function bbcnewsclick(){
    printData(json,"BBC News");
}
function fletcherclick(){
    printData(json,"Fletcher Penney");
}
function wordpressclick(){
    printData(json,"Wordpress");
}
function dockerclick(){
    printData(json,"Docker");
}
function providersclick(){
    document.getElementById("myfeedsdiv").style.display = "none";
    document.getElementById("myproviderssdiv").style.display = "block";    
}

function enabledisable(pName)
{
    var tempprovidername = pName;
    var buttonid = document.getElementById(tempprovidername.replace(/ /g, ''));
    var text = buttonid.innerHTML;
    if(text==="Enabled")
    {
        buttonid.innerHTML="Disabled";
    }
    else if(text==="Disabled")
    {
        buttonid.innerHTML="Enabled";
    }
    var statusp;
    if(buttonid.innerHTML==="Enabled")
    {
        statusp = 1;
    }
    else if (buttonid.innerHTML==="Disabled")
    {
        statusp = 0;
    }
    var req1;
    req1 = new XMLHttpRequest();
    URL = "http://localhost:8080/DatabaseAndWebTechnique/api/post/updateprovidersstatus.php?fname="+pName+"&status="+statusp;
    req1.open("GET", URL , true);
    req1.send();
    window.location.reload(true);
    req1.onload = function () {
        jsonupdate = JSON.parse(requupdate.responseText);
    };
 }

function printData(json,provider) {

    let table = document.getElementById("mytable").getElementsByTagName('tbody')[0];;
    document.getElementById("mytbody").innerHTML="";
    document.getElementById("myproviderssdiv").style.display = "none";
    document.getElementById("myfeedsdiv").style.display = "block";

    for (var i in json) {
        var aTitle = json[i].title;
        var aDate = json[i].date;
        var aLink = json[i].link;
        var aDesc = json[i].descr;
        var aProvider = json[i].provider_name;
        var aDatedetectd = json[i].date_created;

        if(json[i].descr === "")
        {
            aDesc = "Description not available";
        }
        var aLink2 = "<a href="+aLink+">"+aLink+"</a>"

        if(provider===aProvider)
        {
            let row = table.insertRow(-1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);
            var cell6 = row.insertCell(5);
            cell1.innerHTML = aTitle;
            cell2.innerHTML = aDate;
            cell3.innerHTML = aLink2;
            cell4.innerHTML = aDesc;
            cell5.innerHTML = aProvider;
            cell6.innerHTML = aDatedetectd;
            cell3.className = 'mylinks';
        }
        else if(provider==="all")
        {
            let row = table.insertRow(-1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);
            var cell6 = row.insertCell(5);
            cell1.innerHTML = aTitle;
            cell2.innerHTML = aDate;
            cell3.innerHTML = aLink2;
            cell4.innerHTML = aDesc;
            cell5.innerHTML = aProvider;
            cell6.innerHTML = aDatedetectd;
            cell3.className = 'mylinks';
        }
        else
        {
            continue;
        }
    }
}

function printproviders(jsonproviders) {
    let table = document.getElementById("myproviderstable").getElementsByTagName('tbody')[0];
    document.getElementById("myproviderssdiv").style.display = "block";
    document.getElementById("myfeedsdiv").style.display = "none";
    
    for (var i in jsonproviders) {
        var pName    = jsonproviders[i].provider_name;
        var pLink    = jsonproviders[i].provider_link;
        var pDate    = jsonproviders[i].provider_lastupdated;
        var pDate2   = jsonproviders[i].provider_lastattempt;
        var pFormat  = jsonproviders[i].feed_format;
        var pError   = jsonproviders[i].error;
        var pSize    = jsonproviders[i].size;
        var pActive  = jsonproviders[i].active;
        var pLink2   = "<a href="+pLink+">"+pLink+"</a>"

        let row = table.insertRow(-1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        var cell6 = row.insertCell(5);
        var cell7 = row.insertCell(6);
        var cell8 = row.insertCell(7);
        cell1.innerHTML = pName;
        cell2.innerHTML = pLink2;
        cell3.innerHTML = pDate;
        cell4.innerHTML = pDate2;
        cell5.innerHTML = pSize;
        cell6.innerHTML = pFormat;
        cell7.innerHTML = pError;
        cell8.innerHTML = "Enabled";
        console.log(pActive);
        if(pActive == 0)
        {
            cell8.innerHTML = "Disabled";
        }
        else{
            cell8.innerHTML = "Enabled";
        }
        cell8.setAttribute('onclick', 'enabledisable("' + pName + '");');        
        var tempprovider = pName;
        cell8.id = tempprovider.replace(/ /g, '');
        cell2.className = 'mylinks';
    }
}

function refreshfeeds()
{
    var requupdate2;
    requupdate2 = new XMLHttpRequest();
    requupdate2.open("GET", 'http://localhost:8080/DatabaseAndWebTechnique/api/post/updatefeeds.php', true);
    requupdate2.send();
    //window.location.reload(true);
    requupdate2.onload = function () {
        var jsonupdate2 = JSON.parse(requupdate2.responseText);
        updatemessage = jsonupdate2.message;
        var statuses = updatemessage.split(".");
        alert("Update Staus: \n" + statuses[0] + "\n" + statuses[1] + "\n" + statuses[2] + "\n" + statuses[3] + "\n" + statuses[4] + "\n ");
        window.location.reload(true);
    };
}